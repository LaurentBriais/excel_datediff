require "excel_datediff/version"
require 'active_support'
require 'active_support/core_ext'

module ExcelDatediff
  class Error < StandardError; end
  # Your code goes here...
end

require 'excel_datediff/core'