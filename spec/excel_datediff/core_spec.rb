require 'spec_helper'

RSpec.describe ExcelDatediff::Core do

  subject { Object.new.extend described_class }

  let(:test_set) do
    [
      {sd: '31/08/2020', ed: '30/08/2021', dt: :m, res: 12 },
      {sd: '31/08/2020', ed: '31/08/2021', dt: :m, res: 13 },
      {sd: '15/06/2021', ed: '16/06/2021', dt: :m, res: 1 },
      {sd: '15/06/2021', ed: '15/08/2021', dt: :m, res: 3 },
      {sd: '2021-06-15', ed: '2021-07-14', dt: :m, res: 1 },
      {sd: '2021-06-15', ed: '2021-07-16', dt: :m, res: 2 },
      {sd: '2021-06-15', ed: '2021-07-15', dt: :m, res: 2 },
    ]
  end



  it 'should verify some calculations' do
    test_set.each do |ts|
      expect(subject.datedif ts[:sd], ts[:ed], ts[:dt]).to eq ts[:res]
    end
  end

end


